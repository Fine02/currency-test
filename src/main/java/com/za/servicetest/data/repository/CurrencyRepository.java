package com.za.servicetest.data.repository;

import com.za.servicetest.data.entity.Currency;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CurrencyRepository extends JpaRepository<Currency, Long> {
}
