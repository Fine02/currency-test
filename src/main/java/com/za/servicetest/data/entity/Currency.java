package com.za.servicetest.data.entity;

import lombok.*;


import javax.persistence.*;
import java.time.LocalDate;

@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
@Builder
@Entity
@Table(name = "currencies", uniqueConstraints = {
        @UniqueConstraint(columnNames = "cc")
})
public class Currency {

    @Id
    @GeneratedValue
    private Long id;

    @Column(name = "r030")
    private Integer r030;

    @Column(name = "txt")
    private String txt;

    @Column(name = "rate")
    private Double rate;

    @Column(name = "cc")
    private String cc;

    @Column(name = "exchange_date")
    private LocalDate exchangeDate;
}
