package com.za.servicetest.service;

import com.za.servicetest.data.entity.Currency;
import com.za.servicetest.data.repository.CurrencyRepository;
import com.za.servicetest.web.dto.CurrencyDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Slf4j
public class CurrencyService {

    private final CurrencyRepository repository;

    public void saveCurrencies(final List<CurrencyDto> currencyDtos){
        log.info("Start to save new currencies, quantity: {} ", currencyDtos.size());
        repository.saveAll(currencyDtos.stream()
                .map(this::toCurrency)
                .collect(Collectors.toList()));
        log.info("Saved successfully");
    }

    private Currency toCurrency(CurrencyDto dto){
        return Currency.builder()
                .r030(dto.getR030())
                .rate(dto.getRate())
                .txt(dto.getTxt())
                .cc(dto.getTxt())
                .exchangeDate(dto.getExchangeDate()).build();
    }
}
