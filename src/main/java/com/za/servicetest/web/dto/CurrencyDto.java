package com.za.servicetest.web.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Setter
@Getter
@JacksonXmlRootElement(localName = "currency")
public class CurrencyDto {

    @JacksonXmlProperty(localName = "r030")
    private Integer r030;

    @JacksonXmlProperty(localName = "txt")
    private String txt;

    @JacksonXmlProperty(localName = "rate")
    private Double rate;

    @JacksonXmlProperty(localName = "cc")
    private String cc;

    @JacksonXmlProperty(localName = "exchangedate")
    @JsonFormat(pattern = "dd.MM.yyyy")
    private LocalDate exchangeDate;

}
