package com.za.servicetest.web.controller;

import com.za.servicetest.service.CurrencyService;
import com.za.servicetest.web.dto.CurrencyDto;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/currencies")
@RequiredArgsConstructor
public class CurrencyController {

    private final CurrencyService service;

    @PostMapping(consumes = {MediaType.APPLICATION_XML_VALUE, MediaType.TEXT_XML_VALUE})
    public void saveCurrencies(@RequestBody List<CurrencyDto> exchange){
        service.saveCurrencies(exchange);
    }
}
