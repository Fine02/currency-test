package com.za.servicetest;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultMatcher;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class CurrencyControllerIntegrationTest {

    @Autowired
    private MockMvc mockMvc;

    private final String xml = "<exchange>\n" +
            "<currency>\n" +
            "<r030>36</r030>\n" +
            "<txt>Австралійський долар</txt>\n" +
            "<rate>21.1984</rate>\n" +
            "<cc>AUD</cc>\n" +
            "<exchangedate>09.02.2021</exchangedate>\n" +
            "</currency>\n" +
            "<currency>\n" +
            "<r030>124</r030>\n" +
            "<txt>Канадський долар</txt>\n" +
            "<rate>21.6531</rate>\n" +
            "<cc>CAD</cc>\n" +
            "<exchangedate>09.02.2021</exchangedate>\n" +
            "</currency>\n" +
            "</exchange>";

    @Test
    void testControllerPost() throws Exception {
        mockMvc.perform(post("/currencies")
                .contentType(MediaType.TEXT_XML)
                .content(xml))
                .andExpect(ResultMatcher.matchAll(
                        status().isOk()));
    }
}
